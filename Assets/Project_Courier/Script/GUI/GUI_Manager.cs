﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	public class GUI_Manager : MonoBehaviour {
		// Script reference
		private StorePackage storePackage;
        private Game_Manager gameManagerScript;
		//private WaveSpawner waveSpawner;
		private CheckEnemyAlive checkEnemyAlive;
		private Wave_Manager waveManager;

		// Global object reference
		private GameObject gameManager;
		private GameObject player;

		// Component of Win Layout
		public GameObject winLayout;
		public GameObject winFirstButton;
        public int levelToUnlock;

		// Component of Pause Layout
		public GameObject pauseLayout;
		public GameObject pauseFirstButton;
		private GameObject eventSystem;

		// Component of Game Over Layout
		public GameObject gameOverLayout;
		public GameObject gameOverFirstButton;
		private MonsterSpawner[] monsterControl;

        // Component of How To Play Layout
        public GameObject howToLayout;
        public GameObject controllerLayout;
        public GameObject controllerButton;
        public GameObject objectiveLayout;
        public GameObject objectiveButton;

		// Component of Wave Text
		public GameObject waveText;
		public float showWaveText = 5f;
		private float waveTextCountDown;
		private int collectP = 0;
		private int totalP = 0;
		public bool waveTxtStats = true;

		// Component of Collected Package text
		private Button_Manager btnManager;
		public Text collectedText;

		// Component Of Player Score Text
		private int playerScore = 0;
		private bool canGameOver = false;
		public Text scoreText;
		/*[ReadOnly]
		public bool isGameOver = false;
		[ReadOnly]
		public bool isRestarting = false;*/

		// Use this for initialization
		void Start () {
			storePackage = GameObject.FindObjectOfType<StorePackage>();
			waveManager = GameObject.FindObjectOfType<Wave_Manager>();
			checkEnemyAlive = GameObject.FindObjectOfType<CheckEnemyAlive>();
            gameManagerScript = GameObject.FindObjectOfType<Game_Manager>();
			
			btnManager = GameObject.FindObjectOfType<Button_Manager>();

			waveTextCountDown = showWaveText;
		}
		
		// Update is called once per frame
		void Update () {
			waveGUI();
			collectedPackageGUI();
			scoreGUI();
			waveCompleteGUI();
			//checkGameOver();
			gameoverGUI();
            howToPlay();
		}

        private void LateUpdate()
        {
            pauseGUI();
        }

        // Collected Package GUI section here
        public void collectedPackageGUI()
		{
			int waveIndex = waveManager.getWaveIndex();

			if(collectedText == null)
			{
				return;
			}

			if(btnManager.endlessPlay)
			{
				collectedText.text = "Package : " + storePackage.collectedPackage + "/INFINITY";
			}
			else if(waveIndex > 0)
			{
				collectedText.text = "Package : " + collectP + "/" + totalP;
			}
			else 
			{
				collectedText.text = "Package : " + storePackage.collectedPackage + "/" + 
									waveManager.theWave[waveIndex].package;
			}
		}

		// Score GUI section here
		public void scoreGUI() 
		{
			if(scoreText == null)
			{
				return;
			}
			if(playerScore <= 0){
				playerScore = 0;
			}
			scoreText.text = "SCORE : " + playerScore;
		}

		public void setPlayerScore(int score)
		{
			this.playerScore += score;
			if(!canGameOver){
				canGameOver = true;
			}
		}

		public void minPlayerScore(int score){
			if(this.playerScore >= 0){
				this.playerScore -= score;
			}
		}

		public void checkGameOver(){
			if(gameManagerScript.timer <= 0.0f && canGameOver){
				gameManagerScript.isGameOver = true;
                gameManagerScript.isRestarting = false;
			}
		}

		// Wave GUI section here
		public void waveGUI()
		{
			int waveIndex = waveManager.getWaveIndex();

			if(waveText == null)
			{
				return;
			}

			if(btnManager.endlessPlay)
			{
				return;
			}

			if((collectP == totalP && checkEnemyAlive.checkEnemy()) || gameManagerScript.isRestarting) 
			{
				waveTextCountDown -= Time.deltaTime;
				waveTxtStats = true;
				
				
				waveText.GetComponent<UnityEngine.UI.Text>().text = "WAVE " + (waveIndex+1);

				if(waveTextCountDown <= 0)
				{
					waveText.SetActive(true);
				
					float count = -2;

					if(waveTextCountDown <= count)
					{
						waveText.GetComponent<UnityEngine.UI.Text>().text = "START";
					
						count = -4;

						if(waveTextCountDown < count)
						{
                            gameManagerScript.canCount = true;
                            waveText.SetActive(false);
                            gameManagerScript.isRestarting = false;

							totalP = waveManager.theWave[waveIndex].package;
							
							waveTxtStats = false;

							waveTextCountDown = showWaveText;
						}
					}
				}
			}
			else if(waveIndex > 0)
			{
				collectP = storePackage.collectedPackage;
			}
		}

		// Complete Wave GUI section here
		public void waveCompleteGUI()
		{
			int lastWave = waveManager.theWave.Length - 1;

			if(winLayout == null)
			{
				return;
			}

			if(btnManager.endlessPlay)
			{
				return;
			}

			eventSystem = GameObject.Find("EventSystem");
			gameManager = GameObject.Find("Game_Manager");
			player = GameObject.FindGameObjectWithTag("Player");

			if(storePackage.collectedPackage == waveManager.theWave[lastWave].package)
			{
                //PlayerPrefs.SetInt("levelReached", levelToUnlock);
				winLayout.SetActive(true);
				eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().firstSelectedGameObject = winFirstButton;
				eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(winFirstButton);

				gameManager.GetComponent<InputManager>().enabled = false;

				player.GetComponent<CharacterHandleWeapon>().enabled = false;
				player.GetComponent<CharacterHorizontalMovement>().enabled = false;

				Time.timeScale = 0;

				storePackage.collectedPackage = 0;			
			}
		}

		// GameOver GUI Section
		public void gameoverGUI(){
			if(gameOverLayout == null){
				return;
			}
			
			eventSystem = GameObject.Find("EventSystem");
			gameManager = GameObject.Find("Game_Manager");
			player = GameObject.FindGameObjectWithTag("Player");

			if(gameManagerScript.isGameOver && !gameManagerScript.isRestarting && checkEnemyAlive.checkEnemy()){
				
                if (!gameOverLayout.activeInHierarchy)
                {
                    eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().firstSelectedGameObject = gameOverFirstButton;
                    eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(gameOverFirstButton);
                }

				gameOverLayout.SetActive(true);
                gameManager.GetComponent<InputManager>().enabled = false;

                player.GetComponent<CharacterHandleWeapon>().enabled = false;
				player.GetComponent<CharacterHorizontalMovement>().enabled = false;
				//GameObject.FindObjectOfType<CheckEnemyAlive>().killAll();

				storePackage.collectedPackage = 0;
				Time.timeScale = 0;
			}
		}

		// Restart from GameOver
		public void restartGameOver(){
            /*eventSystem = GameObject.Find("EventSystem");
			gameManager = GameObject.Find("Game_Manager");
			player = GameObject.FindGameObjectWithTag("Player");
			monsterControl = GameObject.FindObjectsOfType<MonsterSpawner>();
			Time.timeScale = 1;

			if(gameManagerScript.isGameOver && !gameManagerScript.isRestarting)
            {
				gameOverLayout.SetActive(false);
				player.GetComponent<CharacterHandleWeapon>().enabled = true;
				gameManager.GetComponent<InputManager>().enabled = true;
				player.GetComponent<CharacterHorizontalMovement>().enabled = true;

				//comment below
                foreach(var spawner in monsterControl){
					spawner.restart();
				}
				
				this.playerScore = 0;
				this.canGameOver = false;
                gameManagerScript.isGameOver = false;
                gameManagerScript.isRestarting = true;
                gameManagerScript.canCount = false;
			}*/
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            LevelManager.Instance.GotoLevel(SceneManager.GetActiveScene().name);
        }

        // Pause GUI section here
        public void pauseGUI()
		{
			if(pauseLayout == null)
			{
				return;
			}

			eventSystem = GameObject.Find("EventSystem");
			gameManager = GameObject.Find("Game_Manager");
			player = GameObject.FindGameObjectWithTag("Player");

			if(gameManagerScript.GetPause() && !pauseLayout.activeInHierarchy)
			{
                pauseLayout.SetActive(true);
                eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().firstSelectedGameObject = pauseFirstButton;
				eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(pauseFirstButton);
                
				player.GetComponent<CharacterHandleWeapon>().enabled = false;
				gameManager.GetComponent<InputManager>().enabled = false;
			}
			else if(!gameManagerScript.GetPause() && pauseLayout.activeInHierarchy)
			{
                pauseLayout.SetActive(false);

				player.GetComponent<CharacterHandleWeapon>().enabled = true;
				gameManager.GetComponent<InputManager>().enabled = true;
			}
		}

        // How To Play GUI Section
        public void howToPlay()
        {
            if (pauseLayout == null)
            {
                return;
            }

            if(controllerLayout == null)
            {
                return;
            }

            if(objectiveLayout == null)
            {
                return;
            }

            eventSystem = GameObject.Find("EventSystem");
            gameManager = GameObject.Find("Game_Manager");
            player = GameObject.FindGameObjectWithTag("Player");

            if (gameManagerScript.isTutorial)
            {
                howToLayout.SetActive(true);
                player.GetComponent<CharacterHandleWeapon>().enabled = false;
                gameManager.GetComponent<InputManager>().enabled = false;

                if (controllerLayout.activeSelf)
                {
                    eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().firstSelectedGameObject = controllerButton;
                    eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(controllerButton);
                    //objectiveLayout.SetActive(false);
                }

                if (objectiveLayout.activeSelf)
                {
                    eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().firstSelectedGameObject = objectiveButton;
                    eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(objectiveButton);
                    controllerLayout.SetActive(false);
                }

                Time.timeScale = 0;
            }
            else
            {
                objectiveLayout.SetActive(false);
                controllerLayout.SetActive(false);
                howToLayout.SetActive(false);

                player.GetComponent<CharacterHandleWeapon>().enabled = true;
                gameManager.GetComponent<InputManager>().enabled = true;

                Time.timeScale = 1;
            }
        }

		public int getCollectP()
		{
			return collectP;
		}

		public int getTotalP()
		{
			return totalP;
		}
	}
}
