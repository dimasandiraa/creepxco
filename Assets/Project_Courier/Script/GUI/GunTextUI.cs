﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace MoreMountains.CorgiEngine 
{
	public class GunTextUI : MonoBehaviour {

		private RandomWeapons randomWeapons;
		public GameObject text;

		// Use this for initialization
		void Start () {
			randomWeapons = GameObject.FindObjectOfType<RandomWeapons>();
		}
		
		// Update is called once per frame
		void Update () {
			int weaponIndex = randomWeapons.weaponIndex;

			if(randomWeapons.changeWeapon)
			{
				switch (weaponIndex)
				{
					case 0 :
						text.GetComponent<UnityEngine.UI.Text>().text = "Pistol"; 
						break;
					case 1 : 
						text.GetComponent<UnityEngine.UI.Text>().text = "Machinegun"; 
						break;
					case 2 :
						text.GetComponent<UnityEngine.UI.Text>().text = "Plasma Machinegun";
						break;
					case 3 : 
						text.GetComponent<UnityEngine.UI.Text>().text = "Shotgun"; 
						break;
					case 4 : 
						text.GetComponent<UnityEngine.UI.Text>().text = "Plasma Shotgun"; 
						break;
					case 5 :
						text.GetComponent<UnityEngine.UI.Text>().text = "Rocket Launcher"; 
						break; 
				}
				

				Color color = text.GetComponent<Text>().color;
				color.a = 1f;

				text.GetComponent<Text>().color = color;
		
				text.GetComponent<DOTweenAnimation>().DORestart();
				text.GetComponent<DOTweenAnimation>().DOPlay();

				text.GetComponent<DOTweenPath>().DORestart();
				text.GetComponent<DOTweenPath>().DOPlay();
				
				randomWeapons.changeWeapon = false;
			}
		}
	}
	
}
