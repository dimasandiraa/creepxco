﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class MinusScore : MonoBehaviour {

	public int minScore;
	protected GUI_Manager _guimanager;


	// Use this for initialization
	void Start () {
		_guimanager = GameObject.FindObjectOfType<GUI_Manager>();
		
	}
	
	public void ExecuteMinus(){
		_guimanager.minPlayerScore(minScore);
	}
}
