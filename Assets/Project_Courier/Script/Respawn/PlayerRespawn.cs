﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawn : MonoBehaviour {

	private ChekRespawn cekRespawn;
	public GameObject spawnParticle;
	public float spawnTime;
	private float spawnCountDown;
	private bool toUpdate;

	private void Start() {
		cekRespawn = GameObject.FindObjectOfType<ChekRespawn>();

		spawnCountDown = 0.5f;

		spawnCountDown -= Time.deltaTime;

		if(spawnCountDown <= 0.1f)
		{
			spawnParticle.GetComponent<ParticleSystem>().Play();

			spawnCountDown = spawnTime;
		}
	}

	private void Update() {
		if(cekRespawn.hitObj)
		{	
			spawnCountDown -= Time.deltaTime;

			if(spawnCountDown <= 0.1)
			{
				spawnParticle.GetComponent<ParticleSystem>().Play();

				spawnCountDown = spawnTime;

				cekRespawn.hitObj = false;
			}
		}
		else 
		{
			return;
		}
	}
}
