﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChekRespawn : MonoBehaviour {
	
	public string[] layerName;
	public GameObject tempPoint;
	public bool hitObj = true;
	public GameObject deathParticle;

	private void Start() {
		tempPoint = GameObject.Find("TempPoint");
	}

	private void OnTriggerEnter2D(Collider2D obj) {
		Transform playerPosition;

		for(int i = 0; i < layerName.Length; i++)
		{
			if(obj.gameObject.layer == LayerMask.NameToLayer(layerName[i]))
			{
				playerPosition = gameObject.transform;
				Instantiate(deathParticle, playerPosition.position, playerPosition.rotation);
				
				gameObject.transform.position = tempPoint.transform.position;

				hitObj = true;
			}
		}
	}
}
