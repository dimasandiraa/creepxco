﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class Dialogues : DialogueZone {
	protected Character _character;
	protected PickupPackage _pickupPackage;
    protected Game_Manager _gameManager;
	protected int _badIndex;
	protected int _goodIndex;

    [Multiline]
	public string[] GoodDialogue;
	[Multiline]
	public string[] BadDialogue;
    

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		_character = GameObject.FindObjectOfType<Character>();
		_pickupPackage = GameObject.FindObjectOfType<PickupPackage>();
        _gameManager = GameObject.FindObjectOfType<Game_Manager>();
	}

	/// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
	{
		if(_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Dead && !_playing){
            //CanMoveWhileTalking = true;
			_badIndex = Random.Range(0, BadDialogue.Length);
			StartDialogue();
		}

        /*if(_gameManager.isTutorial && !_playing)
        {
            //CanMoveWhileTalking = false;
            StartDialogue();
        }*/
	}
	
	public override void TriggerButtonAction(){
		if (!CheckNumberOfUses())
		{
			return;
		}
		if (_playing && !ButtonHandled)
		{
			return;
		}
		
		if(_pickupPackage.getPackage){
            //CanMoveWhileTalking = true;
			_goodIndex = Random.Range(0, GoodDialogue.Length);
			StartDialogue();
			ZoneActivated ();
		}
	}

	public override void StartDialogue(){
		// if the button A prompt is displayed, we hide it
		if (_buttonA!=null)
			Destroy(_buttonA);
		
		// if the dialogue zone has no box collider, we do nothing and exit
		if (_collider==null)
			return;	
			
		// if the zone has already been activated and can't be activated more than once.
		if (_activated && !ActivableMoreThanOnce)
			return;
				
		// if the zone is not activable, we do nothing and exit
		if (!_activable)
			return;
			
		// if the player can't move while talking, we notify the game manager
		if (!CanMoveWhileTalking)
		{
			LevelManager.Instance.FreezeCharacters();
			if (ShouldUpdateState)
			{
				_characterButtonActivation.GetComponent<Character>().MovementState.ChangeState(CharacterStates.MovementStates.Idle);
			}
		}
										
		// if it's not already playing, we'll initialize the dialogue box
		if (!_playing)
		{	
			// we instantiate the dialogue box
			GameObject dialogueObject = (GameObject)Instantiate(Resources.Load("GUI/DialogueBox"));
			_dialogueBox = dialogueObject.GetComponent<DialogueBox>();		
			// we set its position
			_dialogueBox.transform.position=new Vector2(_collider.bounds.center.x,_collider.bounds.max.y+DistanceFromTop); 
			// we set the color's and background's colors
			_dialogueBox.ChangeColor(TextBackgroundColor,TextColor);
			// if it's a button handled dialogue, we turn the A prompt on
			_dialogueBox.ButtonActive(ButtonHandled);
			// if font settings have been specified, we set them

			if (BoxesFollowZone)
			{
				_dialogueBox.transform.SetParent (this.gameObject.transform);
			}

			if (TextFont != null)
			{
				_dialogueBox.DialogueText.font = TextFont;
			}

			if (TextSize != 0)
			{
				_dialogueBox.DialogueText.fontSize = TextSize;
			}
			_dialogueBox.DialogueText.alignment = Alignment;

			// if we don't want to show the arrow, we tell that to the dialogue box
			if (!ArrowVisible)
			{
				_dialogueBox.HideArrow();			
			}
			
			// the dialogue is now playing
			_playing=true;
		}
        // we start the random dialogue
        StartCoroutine(PlayRandomDialogue());
        
	}

	protected virtual IEnumerator PlayRandomDialogue(){
		// we check that the dialogue box still exists
		if (_dialogueBox == null) 
		{
			yield break;
		}

		// we check that the dialogue box still exists
		if (_dialogueBox.DialogueText!=null)
		{
			// every dialogue box starts with it fading in
			_dialogueBox.FadeIn(FadeDuration);
			// then we set the box's text with the current dialogue
			if(_character.ConditionState.CurrentState == CharacterStates.CharacterConditions.Dead){
				_dialogueBox.DialogueText.text = BadDialogue[_badIndex];
			}
			else if(_pickupPackage.getPackage){
				_dialogueBox.DialogueText.text = GoodDialogue[_goodIndex];
			}
			//_dialogueBox.DialogueText.text=Dialogue[_currentIndex];
		}
			
		// if the zone is not button handled, we start a coroutine to autoplay the next dialogue
		if (!ButtonHandled)
		{
			StartCoroutine(DestroyDialogue());
		}
	}

	protected virtual IEnumerator DestroyDialogue(){
		yield return _messageDurationWFS;

		_dialogueBox.FadeOut(FadeDuration);
		yield return _transitionTimeWFS;
		
		Destroy(_dialogueBox.gameObject);
		_collider.enabled=false;
		// we set activated to true as the dialogue zone has now been turned on		
		_activated=true;
		// we let the player move again
		if (!CanMoveWhileTalking)
		{
			LevelManager.Instance.UnFreezeCharacters();
		}
		if ((_characterButtonActivation!=null))
		{				
			_characterButtonActivation.InButtonActivatedZone=false;
			_characterButtonActivation.ButtonActivatedZone=null;
		}
		// we turn the zone inactive for a while
		if (ActivableMoreThanOnce)
		{
			_activable=false;
			_playing=false;
			StartCoroutine(ReactivateRandom());
		}
		else
		{
			gameObject.SetActive(false);
		}

		yield break;
	}

	protected virtual IEnumerator ReactivateRandom(){
		yield return _inactiveTimeWFS;
		_collider.enabled=true;
		_activable=true;
		_playing=false;
		_currentIndex = Random.Range(-1, Dialogue.Length);

		if(AlwaysShowPrompt){
			ShowPrompt();
		}
	}
}
