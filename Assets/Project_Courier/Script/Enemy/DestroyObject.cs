﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class DestroyObject : MonoBehaviour {

	GameObject[] theEnemy;
    private Game_Manager gameManager;
	private EnemyCounter countEnemy;

	// Use this for initialization
	void Start () {
        gameManager = GameObject.FindObjectOfType<Game_Manager>();
		countEnemy = GameObject.FindObjectOfType<EnemyCounter>();
	}
	
	// Update is called once per frame
	void Update () {
		if(gameManager.isGameOver){
			killAll();
		}
	}

	public void killAll(){
		Destroy(GameObject.FindGameObjectWithTag("Enemies"));
		countEnemy.enemyCount = 0;
	}

	private void OnTriggerEnter2D(Collider2D enemies) {
		if(enemies.gameObject.tag == "Enemies" && !gameManager.isGameOver)
		{
			theEnemy = GameObject.FindGameObjectsWithTag("Enemies");
			if(!gameManager.isGameOver){
				foreach (var enemy in theEnemy)
				{
					if(enemy == null){
						continue;
					}
					if(enemy.GetComponent<KillableEnemies>().killEnemies)
					{
						Destroy(enemy);
					}
				}
			}
		}	
	}
}
