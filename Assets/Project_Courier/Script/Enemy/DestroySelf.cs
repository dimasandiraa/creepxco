﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class DestroySelf : MonoBehaviour {
	protected Health _healthScript;
	public float minTime = 2.5f;
	public float maxTime = 3.5f;
	

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		_healthScript = gameObject.GetComponent<Health>();
		StartCoroutine(CountdownDestroy());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Sent when another object enters a trigger collider attached to this
	/// object (2D physics only).
	/// </summary>
	/// <param name="other">The other Collider2D involved in this collision.</param>
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Player"){
			_healthScript.Kill();
		}
	}

	protected virtual IEnumerator CountdownDestroy(){
		yield return new WaitForSeconds(Random.Range(minTime, maxTime));
		_healthScript.Kill();
	}
}
