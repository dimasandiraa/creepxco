﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class CheckEnemyAlive : MonoBehaviour {
	// You can change this with another tag name
	public string enemyTag = "Enemies";

	// Save all enemy Game Object
	private GameObject[] enemies;

	public bool checkEnemy()
	{
		if(GameObject.FindGameObjectWithTag(enemyTag) == null)
		{
			enemies = GameObject.FindGameObjectsWithTag(enemyTag);
			return true;
		}

		return false;
	}
}
