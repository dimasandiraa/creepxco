﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
	public class EnemyScore : MonoBehaviour {

		public int score;

		private GUI_Manager guiManager;

		// Use this for initialization
		void Start () {
			guiManager = GameObject.FindObjectOfType<GUI_Manager>();
		}

		void OnTriggerEnter2D(Collider2D obj)
		{
			if(obj.gameObject.layer == LayerMask.NameToLayer("PlayerProjectiles"))
			{
				guiManager.setPlayerScore(score);
			}
		}
	}
}
