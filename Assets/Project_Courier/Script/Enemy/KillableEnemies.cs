﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillableEnemies : MonoBehaviour {

	public bool killEnemies = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter2D(Collider2D obj) {
		if(obj.gameObject.name == "DestroyArea")
		{
			killEnemies = true;
		}
	}
}
