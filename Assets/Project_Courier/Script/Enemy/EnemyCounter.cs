﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class EnemyCounter : MonoBehaviour {
    /*This script's function is to counting how much enemy in the map, and restrict the max number of enemies in the map */


    public int maxEnemyTutorial;
    public int maxEnemyWave;
    [System.NonSerialized]
    public int maxEnemy;
    [System.NonSerialized]
    public int enemyCount;

    private Game_Manager gameManager;

    private void Start()
    {
        gameManager = GameObject.FindObjectOfType<Game_Manager>();
    }

    private void Update()
    {
        checkMaxEnemy();
    }

    public void checkMaxEnemy()
    {
        if (gameManager.isTutorial)
        {
            maxEnemy = maxEnemyTutorial;
        }
        else
        {
            maxEnemy = maxEnemyWave;
        }
    }

    public void increaseEnemy()
    {
        enemyCount++;
    }

    public void decreaseEnemy()
    {
        enemyCount--;
    }

    public int getCount()
    {
        return enemyCount;
    }
}
