﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPath : MonoBehaviour {

	public enum PathTypes
	{
		LINEAR,
		LOOP
	}

	public PathTypes pathTypes;
	public int movementDirection = 1;
	public int moveToPath = 0;
	public Transform[] pathSequence;

	public void OnDrawGizmos() 
	{
		if(pathSequence == null || pathSequence.Length < 2)
		{
			return;
		}	

		for(int i = 1; i < pathSequence.Length; i++)
		{
			Gizmos.DrawLine(pathSequence[i-1].position, pathSequence[i].position);
		}

		if(pathTypes == PathTypes.LOOP)
		{
			Gizmos.DrawLine(pathSequence[0].position, pathSequence[pathSequence.Length - 1].position);
		}
	}

	public IEnumerator<Transform> getPathToNextPoint()
	{
		if(pathSequence == null || pathSequence.Length <= 0)
		{
			yield break;
		}

		while(true)
		{
			yield return pathSequence[moveToPath];

			if(pathSequence.Length == 1)
			{
				continue;
			}

			if(pathTypes == PathTypes.LINEAR)
			{
				if(moveToPath <= 0)
				{
					movementDirection = 1;
				}
				else if(moveToPath >= pathSequence.Length - 1)
				{
					movementDirection = -1;
				}
			}

			moveToPath += movementDirection;

			if(pathTypes == PathTypes.LOOP)
			{
				if(moveToPath >= pathSequence.Length)
				{
					moveToPath = 0;
				}
				else if(moveToPath < 0) 
				{
					moveToPath = pathSequence.Length - 1;
				}
			}
		}
	}
}
