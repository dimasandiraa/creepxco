﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour {

	public enum MovementType
	{
		MOVETOWARDS,
		LERPTOWARDS
	}

	public MovementType movementType = MovementType.MOVETOWARDS; 
	public MovementPath movementPath;
	public float speed = 1;
	public float maxDistaceToGoal = .1f;

	private IEnumerator<Transform> pointInPath;

	// Use this for initialization
	void Start () 
	{
		if(movementPath == null)
		{
			Debug.Log("Movement Path is Empty");
			return;
		}	

		pointInPath = movementPath.getPathToNextPoint();

		pointInPath.MoveNext(); 

		if(pointInPath.Current == null)
		{
			Debug.Log("Path must have point to Follow");
			return;
		}

		transform.position = pointInPath.Current.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(pointInPath == null || pointInPath.Current == null)
		{
			return;
		}

		if(movementType == MovementType.MOVETOWARDS)
		{
			transform.position = Vector3.MoveTowards(transform.position, pointInPath.Current.position, Time.deltaTime * speed);
		}

		if(movementType == MovementType.LERPTOWARDS)
		{
			transform.position = Vector3.Lerp(transform.position, pointInPath.Current.position, Time.deltaTime * speed);
		}

		var distaceSquared = (transform.position - pointInPath.Current.position).sqrMagnitude;

		if(distaceSquared < maxDistaceToGoal * maxDistaceToGoal)
		{
			pointInPath.MoveNext();
		}
	}
}
