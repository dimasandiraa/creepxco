﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPath : MonoBehaviour {

	public enum Direction {LEFT,RIGHT}

	private SpriteRenderer spriteRenderer;
	Rigidbody2D rigidBody2D;

	public Direction direction = Direction.RIGHT; 
	public float enemySpeed = 5f;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
		rigidBody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter2D(Collider2D trigger) {
		if(trigger.gameObject.layer == LayerMask.NameToLayer("Platforms") || trigger.gameObject.layer == LayerMask.NameToLayer("MovingPlatforms"))
		{
			if(direction == Direction.LEFT)
			{
				rigidBody2D.velocity = new Vector2(enemySpeed*-1, rigidBody2D.velocity.y);
			} 
			else 
			{
				rigidBody2D.velocity = new Vector2(enemySpeed, rigidBody2D.velocity.y);
			}
		}

		if(trigger.gameObject.tag == "direction")
		{	
			if(direction == Direction.RIGHT)
			{
				direction = Direction.LEFT;

				spriteRenderer.flipX = false;
				rigidBody2D.velocity = new Vector2(enemySpeed*-1, rigidBody2D.velocity.y);
			} 
			else
			{
				direction = Direction.RIGHT;

				spriteRenderer.flipX = true;
				rigidBody2D.velocity = new Vector2(enemySpeed, rigidBody2D.velocity.y);
			} 
		}
	}
}
