﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using UnityEngine.UI;
using DG.Tweening;

public class ComboManager : MonoBehaviour {

    [Header("Combo Manager")]
    ///add this script to count the combo of the weapon
    public float dropComboTime = 1.5f;
    [ReadOnly]
    public float lastComboTime = 0;

    public int comboCount = 0;
    public int showTextAfter = 2;

    public bool droppableCombo = true;

    public GameObject ComboText;
    public string[] stylishRank;

    protected bool _countActive = false;

    protected virtual void Update()
    {
        ResetCombo();
    }

    protected virtual void ResetCombo()
    {
        if(droppableCombo && _countActive)
        {
            lastComboTime += Time.deltaTime;
            if(lastComboTime > dropComboTime)
            {
                _countActive = false;
                comboCount = 0;
                ComboText.GetComponent<UnityEngine.UI.Text>().text = "";
                //ComboText.GetComponent<DOTweenAnimation>().DOKill();
            }
        }
    }

    public void ComboCount()
    {
        _countActive = true;
        comboCount += 1;
        lastComboTime = 0;

        if(comboCount > showTextAfter)
        {
            if(ComboText == null)
            {
                return;
            }
            else
            {
                if(comboCount > 3)
                {
                    ComboText.GetComponent<UnityEngine.UI.Text>().text = stylishRank[Random.Range(0, stylishRank.Length)];
                }
                else
                {
                    ComboText.GetComponent<UnityEngine.UI.Text>().text = comboCount + " Combos";
                }
                ComboText.GetComponent<DOTweenAnimation>().DORestart();
                ComboText.GetComponent<DOTweenAnimation>().DOPlay();
            }
        }
    }
}
