﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{	
public class RandomWeapons : MonoBehaviour {

	public Weapon[] weapons;
	public bool changeWeapon = false;
	public int weaponIndex;
	
	private CharacterHandleWeapon handleWeapon;
	private PickupPackage pickupPackage;

	// Use this for initialization
	void Start () {
		handleWeapon = GameObject.FindObjectOfType<CharacterHandleWeapon>();
		pickupPackage = GameObject.FindObjectOfType<PickupPackage>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D player)
	{
		if(player.gameObject.tag == "Player" && pickupPackage.getPackage)
		{
			weaponIndex = Random.Range(0, weapons.Length);

			changeWeapon = true;

			handleWeapon.ChangeWeapon(weapons[weaponIndex], null);
		}
	}
}
}
