﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayParticleSystem : MonoBehaviour {
	public string tagName;
	private bool finishPlay;

	private void Update() {
		if(finishPlay)
		{
			if(!gameObject.GetComponent<ParticleSystem>().isPlaying)
			{
				Destroy(gameObject);
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D obj) {
		if(obj.gameObject.tag.Equals(tagName) && !finishPlay)
		{
			gameObject.GetComponent<ParticleSystem>().Play();
			
			finishPlay = true;
		}
	}

	
}
