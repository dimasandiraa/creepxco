﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
public class ResetPackage : MonoBehaviour {

	private PickupPackage pickupPackage;
	private PackageSpawner2 packageSpawner;

	// Use this for initialization
	void Start () {
		pickupPackage = GameObject.FindObjectOfType<PickupPackage>();
		packageSpawner = GameObject.FindObjectOfType<PackageSpawner2>();
	}

	void OnTriggerEnter2D(Collider2D obj)
	{
		if(obj.gameObject.tag == "destroyArea" || obj.gameObject.tag == "Enemies" || obj.gameObject.tag == "trap")
		{
			pickupPackage.getPackage = false;
			packageSpawner.canSpawn = true;
		}
	}
}
}
