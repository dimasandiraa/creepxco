﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupPackage : MonoBehaviour {

	public bool getPackage = false;
	public AudioSource packageSound;
	GameObject[] spawnedPackage;

	void OnTriggerEnter2D(Collider2D obj)
	{
		if(obj.gameObject.tag == "package")
		{
			getPackage = true;
			
			spawnedPackage = GameObject.FindGameObjectsWithTag("package");
			
			foreach (var thePackage in spawnedPackage)
			{
				if(thePackage.activeInHierarchy)
				{
					Destroy(thePackage);
				}
			}
		}
	}
}
