﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
public class StorePackage : MonoBehaviour {
	private PickupPackage pickupPackage;
	private PackageSpawner2 packageSpawner;

	public int collectedPackage = 0;
	private int dummyCollected;

	// Use this for initialization
	void Start () {
		pickupPackage = GameObject.FindObjectOfType<PickupPackage>();
		packageSpawner = GameObject.FindObjectOfType<PackageSpawner2>();

		dummyCollected = collectedPackage;
	}

	private void Update() {
		
	}
	
	public void setDummyPackage(int collectP)
	{
		this.dummyCollected = collectP;
	}

	public int getDummyPackage()
	{
		return dummyCollected;
	}

	void OnTriggerEnter2D(Collider2D player)
	{
		if(player.gameObject.tag == "Player" && pickupPackage.getPackage)
		{
			collectedPackage += 1;
			
			pickupPackage.getPackage = false;
			packageSpawner.canSpawn = true;
		}
	}
}
}