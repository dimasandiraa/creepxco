﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
    public class Wave_Manager : MonoBehaviour {
	    // User input for customize the wave
	    [System.Serializable]
	    public class Waves
	    {
		    public string name;
		    public int package;
		    public bool waveComplete = false;
	    }
	    public Waves[] theWave;
        public int tutorialWave;
	
	    // Variable that has an index of wave
	    private int waveIndex = 0;
	    // Variable that use for start the wave
	    public float startWaveAt;

	    // Other Script references
	    private StorePackage storePackage;
        private Game_Manager gameManager;

	    // Use this for initialization
	    void Start () {
		    storePackage = GameObject.FindObjectOfType<StorePackage>();
            gameManager = GameObject.FindObjectOfType<Game_Manager>();
	    }
	
	    // Update is called once per frame
	    void Update () {
		    if(waveIndex == (theWave.Length-1))
		    {
			    waveIndex = theWave.Length-1;
		    }
		    else if(storePackage.collectedPackage == theWave[waveIndex].package && gameManager.timer >= 0.0f)
		    {
			    theWave[waveIndex].waveComplete = true;
                if(waveIndex+1 == tutorialWave)
                {
                    gameManager.isTutorial = false;
                }
			
			    waveIndex++;
		    }
	    }

        public void setWaveIndex(int newIndex)
        {
            waveIndex = newIndex;
        }

	    public int getWaveIndex()
	    {
		    return waveIndex;
	    }
    }
}