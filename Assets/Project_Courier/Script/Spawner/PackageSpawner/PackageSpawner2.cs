﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
    public class PackageSpawner2 : MonoBehaviour {

	    public GameObject[] package;
	    public Transform[] spawnLocation;

	    public float spawnPackageAt = 7f;
	    private float spawnCountDown;
	    public bool canSpawn = true;

	    private GUI_Manager waveText;
        private Game_Manager gameManager;

	    private PickupPackage pickupPackage;
	    private MonsterSpawner monsterSpawner;
	    private StorePackage storePackage;
	    private Wave_Manager waveManager;

	    void Start () {
		    pickupPackage = GameObject.FindObjectOfType<PickupPackage>();
		    monsterSpawner = GameObject.FindObjectOfType<MonsterSpawner>();
		    waveText = GameObject.FindObjectOfType<GUI_Manager>();
		    storePackage = GameObject.FindObjectOfType<StorePackage>();
		    waveManager = GameObject.FindObjectOfType<Wave_Manager>();
            gameManager = GameObject.FindObjectOfType<Game_Manager>();

		    spawnCountDown = spawnPackageAt;
	    }
	
	    // Update is called once per frame
	    void Update () {
		    float countDown = monsterSpawner.getCountDown();

		    int packageIndex = Random.Range(0, package.Length);
		    int locationIndex = Random.Range(0, spawnLocation.Length);

		    if(!pickupPackage.getPackage && /*!waveText.waveTxtStats*/ waveText.getCollectP() != waveText.getTotalP())
		    {
			    if(canSpawn && GameObject.FindGameObjectWithTag("package") == null && countDown < 0.1 && !gameManager.isGameOver)
			    {
				    spawnCountDown -= Time.deltaTime;

				    if(spawnCountDown <= 0)
				    {
					    Instantiate(package[packageIndex], spawnLocation[locationIndex].position, spawnLocation[locationIndex].rotation);
					    canSpawn = false;
					    spawnCountDown = spawnPackageAt;
				    }
			    }
			    else 
			    {
				    spawnCountDown = spawnPackageAt;
			    }
		    }

		    //Debug.Log(countDown);

		    /*if(gameManager.isGameOver && GameObject.FindGameObjectWithTag("package") != null){
			    Destroy(GameObject.FindGameObjectWithTag("package"));
			    spawnCountDown = spawnPackageAt;
		    }*/

            if(GameObject.FindGameObjectWithTag("package") != null)
            {
                if (gameManager.isGameOver)
                {
                    Destroy(GameObject.FindGameObjectWithTag("package"));
                    spawnCountDown = spawnPackageAt;
                }
            }

		    /*if(GameObject.FindGameObjectWithTag("package") == null){
			    canSpawn = true;
			
		    }*/

		    /*if(waveText.isRestarting){
			    //canSpawn = true;
			    spawnCountDown = spawnPackageAt;
		    }*/
	    }
    }
}