﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
	public class MonsterSpawner : MonoBehaviour {
		
		// Other script reference
		private Wave_Manager waveManager;
		private CheckEnemyAlive checkEnemyAlive;
		private StorePackage storePackage;
        private EnemyCounter enemyCount;
		private Game_Manager gameManager;

		// Enum to define what kind of monster
		public enum MonsterType{FLYINGMONSTER, LANDMONSTER}
		public MonsterType monsterType = MonsterType.LANDMONSTER;

		// Enum to control enemy spawn
		public enum SpawnState {SPAWNING, COUNTING, WAITING}
		public SpawnState state = SpawnState.WAITING;

		/* 
			Class with Variable that defines the enemy
			
			Note : 	The size of Monster must be same like the length of wave in
					Wave_Manager, for the size of enemies you can fill as you like
		*/
		[System.Serializable]
		public class Monster
		{
			public string name = "Wave x";
			public GameObject[] enemies;
			public float spawnRate;
		}
		public Monster[] monster;
		
		// variable to get wave index
		private int waveIndex = 0;
		// variable that has the time from Wave_Manager
		private float spawnCountdown;

		// Use this for initialization
		void Start () {
			waveManager = GameObject.FindObjectOfType<Wave_Manager>();
			checkEnemyAlive = GameObject.FindObjectOfType<CheckEnemyAlive>();
			storePackage = GameObject.FindObjectOfType<StorePackage>();
            enemyCount = GameObject.FindObjectOfType<EnemyCounter>();
			gameManager = GameObject.FindObjectOfType<Game_Manager>();

			spawnCountdown = waveManager.startWaveAt;
		}
		
		// Update is called once per frame
		void Update () {
            //Debug.Log(state);

			if(state == SpawnState.COUNTING)
			{
				spawnCountdown = waveManager.startWaveAt;

				if((checkEnemyAlive.checkEnemy() && waveManager.theWave[waveIndex].waveComplete) || gameManager.isRestarting)
				{
					storePackage.collectedPackage = 0;

					waveIndex = waveManager.getWaveIndex();
					state = SpawnState.WAITING;
				}
			}	

			if(state == SpawnState.WAITING)
			{
				spawnCountdown -= Time.deltaTime;

				if(spawnCountdown <= 0)
				{
					StartCoroutine(spawning());
				}
			}
		}

		IEnumerator spawning()
		{
			state = SpawnState.SPAWNING;

			for(int i = 0; i < 2; i++)
			{
				if(waveManager.theWave[waveIndex].waveComplete || gameManager.isGameOver)
				{
					state = SpawnState.COUNTING;

					yield break;
				}
				else
				{
					spawnMonster();

					i = 0;

					yield return new WaitForSeconds(monster[waveIndex].spawnRate);
				}
			}
		}

		public void spawnMonster()
		{
			int monsterIndex = Random.Range(0, (monster[waveIndex].enemies.Length-1));

			if(monster[waveIndex].enemies[monsterIndex] == null || enemyCount.maxEnemy == enemyCount.getCount())
			{
				return;
			}
			else 
			{
				Instantiate(monster[waveIndex].enemies[monsterIndex], gameObject.transform.position, gameObject.transform.rotation);
                enemyCount.increaseEnemy();
			}
		}

		public float getCountDown()
		{
			return spawnCountdown;
		}

		public void restart(){
			state = SpawnState.WAITING;
			spawnCountdown = waveManager.startWaveAt;
			storePackage.collectedPackage = 0;
			waveIndex = waveManager.getWaveIndex();
		}
	}
}