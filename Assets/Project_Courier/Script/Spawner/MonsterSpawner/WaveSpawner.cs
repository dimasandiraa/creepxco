﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MoreMountains.CorgiEngine
{
	public class WaveSpawner : MonoBehaviour {

		public enum SpawnState {SPAWNING, COUNTING, WAITING}
		public SpawnState state = SpawnState.COUNTING;

		[System.Serializable]
		public class Wave
		{
			public string name;
			public Transform[] enemyRight;
			public Transform[] enemyLeft;
			public float spawnRate; 
			public int finishWave; 
		}
		public Wave[] waves;

		public Transform[] spawnPointRight;
		public Transform[] spawnPointLeft;

		public int nextWave = 0;
		public int theWave;

		public float startWaveAt = 5f;
		public float waveCountDown;

		private bool waveClear = false;

		private StorePackage storePackage;

		void Start() 
		{
			storePackage = GameObject.FindObjectOfType<StorePackage>();

			waveCountDown = startWaveAt;
		}

		void Update() 
		{
			if(state == SpawnState.COUNTING)
			{
				waveCountDown = startWaveAt;

				if(checkEnemy() && waveClear)
				{
					waveComplete();

					if(!waveClear)
					{
						waves[nextWave].spawnRate += 0.1f;
						state = SpawnState.WAITING;
					}
				}
			}

			if(state == SpawnState.WAITING)
			{
				waveCountDown -= Time.deltaTime;

				if(waveCountDown <= 0)
				{
					StartCoroutine(spawnWave(waves[nextWave]));
				}
			}
		}

		// check the enemy life or not
		public bool checkEnemy()
		{
			if(GameObject.FindGameObjectWithTag("Enemies") == null)
			{
				return true;
			}

			return false;
		}

		// Spawing the enemy
		IEnumerator spawnWave(Wave _wave)
		{
			state = SpawnState.SPAWNING;

			for(int i = 0; i < 2; i++)
			{
				if(storePackage.collectedPackage == _wave.finishWave)
				{
					state = SpawnState.COUNTING;
					
					if(nextWave + 1 > waves.Length - 1)
					{
						// nextWave = nextWave;	
					}
					else 
					{
						nextWave++;
					}

					yield break;
				}
				else 
				{
					spawnEnemy();

					i = 0;
		
					yield return new WaitForSeconds(1f/_wave.spawnRate);			
				}
			}
		}

		// Spawn enemy Method
		private void spawnEnemy()
		{
			int enemyIndexR = Random.Range(0, waves[nextWave].enemyRight.Length);
			int enemyIndexL = Random.Range(0, waves[nextWave].enemyLeft.Length);

			if(spawnPointRight[0] == null)
			{
				return;
			}
			else 
			{
				for(int i = 0; i < spawnPointRight.Length; i++)
				{
					Instantiate(waves[nextWave].enemyRight[enemyIndexR], spawnPointRight[i].position, spawnPointRight[i].rotation);	
				}
			}

			if(spawnPointLeft[0] == null)
			{
				return;
			}
			else
			{
				for(int i = 0; i < spawnPointLeft.Length; i++)
				{
					Instantiate(waves[nextWave].enemyLeft[enemyIndexL], spawnPointLeft[i].position, spawnPointLeft[i].rotation);	
				}
			}
		}
		
		// Something to do after wave complete
		private void waveComplete()
		{
			storePackage.collectedPackage = 0;
			theWave = nextWave;
			waveClear = false;
		}
		
		public void setWaveClear(bool waveClear)
		{
			this.waveClear = waveClear;
		}
	}
}
