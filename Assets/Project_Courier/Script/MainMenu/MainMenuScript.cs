﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.RainMaker;

public class MainMenuScript : MonoBehaviour {

    public RainScript2D rainScript;

	// Use this for initialization
	void Start () {
        rainScript.RainIntensity = Random.Range(0, 2);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
