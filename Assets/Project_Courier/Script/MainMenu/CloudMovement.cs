﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudMovement : MonoBehaviour {

    private Vector3 _endPos;
    private bool isMove = false;
    private RectTransform rectTrans;

    public float speed;
    public float endPosX = 1201;
    public float minY = -30;
    public float maxY = 500;


	// Use this for initialization
	void Start () {
        rectTrans = GetComponent<RectTransform>();
        _endPos = new Vector3(endPosX, this.rectTrans.anchoredPosition.y, 0);
        isMove = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (isMove)
        {
            Move();
        }
        else
        {
            return;
        }
    }

    private void Move()
    {
        if(rectTrans.anchoredPosition.x < endPosX)
        {
            rectTrans.anchoredPosition = Vector3.MoveTowards(rectTrans.anchoredPosition, _endPos, (speed * Time.deltaTime));
        }
        else //if(rectTrans.anchoredPosition.x == endPosX)
        {
            ResetCloud();
        }
    }

    private void ResetCloud()
    {
        rectTrans.anchoredPosition = new Vector2(-(endPosX), Random.Range(minY, maxY));
        _endPos = new Vector3(endPosX, this.rectTrans.anchoredPosition.y, 0);
    }
}
