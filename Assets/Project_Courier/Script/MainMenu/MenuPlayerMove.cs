﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace MoreMountains.CorgiEngine
{
    public class MenuPlayerMove : Button_Manager
    {

        private bool move = false;
        public Animator m_play;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (!move)
            {
                return;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x + 5, transform.position.y, transform.position.z), (3 * Time.deltaTime));
                
            }
        }

        public void doMove()
        {
            transform.Rotate(0, 180, 0);
            move = true;
            
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Portal")
            {
                gameObject.SetActive(false);
                move = false;
                arenaSelectScene();
            }
        }
    }
}
