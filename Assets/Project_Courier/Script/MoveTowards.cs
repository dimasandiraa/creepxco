﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowards : MonoBehaviour {

    public bool randomX = false;
    public bool randomY = false;

    public float randomMin = 0;
    public float randomMax = 1;
    public float speed = 2.5f;

    public Vector3 startPoint = new Vector3(0, 0, 0);
    public Vector3 targetPoint = new Vector3(0, 0, 0);
    

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Move();
	}

    protected virtual void Move()
    {
        if(transform.position.x > targetPoint.x || transform.position.y > targetPoint.y)
        {
            if (randomY)
            {
                transform.position = new Vector3(startPoint.x, Random.Range(randomMin, randomMax), transform.position.z);
            }
            else if(randomX)
            {
                transform.position = new Vector3(Random.Range(randomMin, randomMax), startPoint.y, transform.position.z);
            }
            else
            {
                transform.position = new Vector3(startPoint.x, startPoint.y, startPoint.z);
            }
        }

        transform.position = Vector3.MoveTowards(transform.position, new Vector3(targetPoint.x, targetPoint.y, targetPoint.z), speed * Time.deltaTime);
        
    }
}
