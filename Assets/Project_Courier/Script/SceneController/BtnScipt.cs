﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;
using MoreMountains.CorgiEngine;

public class BtnScipt : MonoBehaviour {

	public string lvlName;

	public void changeArena()
	{
        SceneManager.LoadScene(lvlName);
        //LevelManager.Instance.GotoLevel(lvlName);
		Time.timeScale = 1;
	}
}
