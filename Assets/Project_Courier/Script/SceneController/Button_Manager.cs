﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
	public class Button_Manager : MonoBehaviour
    {

		// Global public Object reference
		public string arenaSceneName;
		public string mainMenuSceneName;
		public string[] arenaName;
		public GameObject eventSystem;

		// Global private Object reference
		private GameObject gameManager;
		private GameObject player;

		// Keep Playing function reference
		public bool endlessPlay = false;

		// Arena Select reference
		public GameObject selectLvlBtn;

		// Main Menu reference
		public GameObject mainMenuBtn;

		void Start()
		{
			if(selectLvlBtn == null)
			{
				return;
			}
			else
			{
				eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().firstSelectedGameObject = selectLvlBtn;
				eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(selectLvlBtn);
			}

			if(mainMenuBtn == null)
			{
				return;
			}
			else
			{
				eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().firstSelectedGameObject = mainMenuBtn;
				eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(mainMenuBtn);
			}
		}
		
		// Resume Game function
		public void resume()
		{
			gameManager = GameObject.Find("Game_Manager");
			player = GameObject.FindGameObjectWithTag("Player");

			player.GetComponent<CharacterHandleWeapon>().enabled = true;
			gameManager.GetComponent<InputManager>().enabled = true;
            gameManager.GetComponent<Game_Manager>().SetPause(false);

			gameManager.GetComponent<GUI_Manager>().pauseLayout.SetActive(false);

			Time.timeScale = 1;
		}

		// Go to arena select scene
		public void arenaSelectScene()
		{
            LevelManager.Instance.GotoLevel(arenaSceneName);
			endlessPlay = false;

			Time.timeScale = 1;
		}

		// Go to Main menu Scene
		public void mainMenuScene()
		{
			//SceneManager.LoadScene(mainMenuSceneName);
            LevelManager.Instance.GotoLevel(mainMenuSceneName);
			endlessPlay = false;

			Time.timeScale = 1;
		}

		public void keepPlaying()
		{
			gameManager = GameObject.Find("Game_Manager");
			player = GameObject.FindGameObjectWithTag("Player");

			endlessPlay = true;

			player.GetComponent<CharacterHandleWeapon>().enabled = true;
			player.GetComponent<CharacterHorizontalMovement>().enabled = true;

			gameManager.GetComponent<InputManager>().enabled = true;
			gameManager.GetComponent<WaveSpawner>().waves[gameManager.GetComponent<WaveSpawner>().waves.Length-1].finishWave = -1;
			gameManager.GetComponent<GUI_Manager>().winLayout.SetActive(false);

			Time.timeScale = 1;
		}

		// Exit the Game function
		public void exitGame()
		{
			Application.Quit();
		}
	}
}
