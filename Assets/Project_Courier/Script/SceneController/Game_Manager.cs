﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine
{
    public class Game_Manager : MonoBehaviour
    {
        public float timer;
        public Text timerText;
        [ReadOnly]
        public bool isTutorial = false;
        [ReadOnly]
        public bool isGameOver = false;
        [ReadOnly]
        public bool isRestarting = false;
        [ReadOnly]
        public bool canCount = false;

        private float sec;
        private int min;
        private bool isPause = false;

        private void Start()
        {
            //guiManager = FindObjectOfType<GUI_Manager>();
        }

        private void Update()
        {
            TimeTick();
            //Debug.Log(Time.timeScale);
        }

        private void LateUpdate()
        {
            CheckPause();
        }

        public bool GetPause()
        {
            return isPause;
        }

        public void SetPause(bool newPause)
        {
            isPause = newPause;
        }

        private void CheckPause()
        {
            if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("Player1_Pause")) && !isPause)
            {
                if (!isPause)
                {
                    if (!isTutorial)
                    {
                        isPause = true;
                    }
                }
                else if (isPause)
                {
                    isPause = false;
                }
                Debug.Log(isPause);
                Debug.Log(Time.timeScale);
            }
        }

        private void TimeTick()
        {
            if (isPause || isTutorial)
            {
                Time.timeScale = 0;
                return;
            }

            Time.timeScale = 1;
            if (timer >= 0.0f && !isGameOver && !isPause)
            {
                if (canCount)
                {
                    timer -= Time.deltaTime;
                }

                if (timer >= 60.0f)
                {
                    min = (int)timer / 60;
                    sec = timer % 60;
                }
                else
                {
                    min = 0;
                    sec = timer;
                }
                timerText.text = (min.ToString("D1") + " : " + sec.ToString("F"));
            }
            else if (timer <= 0.0f)
            {
                canCount = false;
                timerText.text = "0 : 00.00";
                isGameOver = true;
            }
            Debug.Log(Time.timeScale);
        }

        public void TutorialOn()
        {
            isTutorial = true;
        }

        public void TutorialOff()
        {
            isTutorial = false;
        }
    }
}