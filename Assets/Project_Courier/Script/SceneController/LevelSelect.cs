﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelSelect : MonoBehaviour {

    public Button[] levelButtons;
    public RectTransform arrow;
    private int levelReached;

	// Use this for initialization
	void Start () {
        
    }

    private void Awake()
    {
        //levelReached = PlayerPrefs.GetInt("levelReached", 1);
        //CheckInterractable();
        ArrowCheck();
    }

    private void Update()
    {
        ArrowCheck();
    }

    private void CheckInterractable()
    {
        for (int i = 0; i < levelButtons.Length; i++)
        {
            if ((i + 1 > levelReached) && (levelButtons[i].gameObject.name != "MainMenu"))
            {
                levelButtons[i].interactable = false;
            }
        }
        levelButtons[levelButtons.Length - 1].interactable = true;
    }

    private void ArrowCheck()
    {
        for(int i = 0; i < levelButtons.Length; i++)
        {
            if(EventSystem.current.currentSelectedGameObject == levelButtons[i].gameObject)
            {
                //arrow.anchoredPosition = new Vector3(levelButtons[i].GetComponent<RectTransform>().anchoredPosition.x, arrow.anchoredPosition.y);
                levelButtons[i].GetComponentInChildren<Animator>().enabled = true;
            }
            else
            {
                levelButtons[i].GetComponentInChildren<Animator>().enabled = false;
                levelButtons[i].GetComponentInChildren<Text>().color = new Color(0, 0, 0);
            }
        }
    }
}
